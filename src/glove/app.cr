require "lib_glfw"
require "lib_glew"
require "gl"

require "./shader"
require "./shader_program"

class App
  abstract def update

  abstract def render

  def cleanup
  end

  getter :window

  def initialize(@width, @height, @title)
    unless LibGLFW.init
      raise "LibGLFW.init failed"
    end

    error_callback = ->(error : Int32, string : UInt8*) do
      STDERR.puts "GLFW error ##{error}: #{String.new(string)}"
      exit 1
    end
    LibGLFW.set_error_callback(error_callback)

    LibGLFW.window_hint(LibGLFW::SAMPLES, 4)
    LibGLFW.window_hint(LibGLFW::CONTEXT_VERSION_MAJOR, 3)
    LibGLFW.window_hint(LibGLFW::CONTEXT_VERSION_MINOR, 3)
    LibGLFW.window_hint(LibGLFW::OPENGL_FORWARD_COMPAT, LibGL::TRUE)
    LibGLFW.window_hint(LibGLFW::OPENGL_PROFILE, LibGLFW::OPENGL_CORE_PROFILE)

    @window = LibGLFW.create_window(@width, @height, @title, nil, nil)
    LibGLFW.set_current_context(@window)

    LibGLFW.set_window_user_pointer(@window, self as Void*)

    GLEW.experimental = LibGL::TRUE
    GL.check_error("before GLEW.init")
    unless GLEW.init == GLEW::OK
      raise "GLEW.init failed"
    end
    GL.check_error("after GLEW.init")
    puts "The previous INVALID_ENUM error (if any) can be safely ignored."

    LibGLFW.set_input_mode(@window, LibGLFW::CURSOR, LibGLFW::CURSOR_DISABLED)

    LibGL.enable(LibGL::MULTISAMPLE)
    LibGL.enable(LibGL::DEPTH_TEST)

    LibGL.depth_func(LibGL::LESS)
  end

  def run
    before = LibGLFW.get_time
    LibGL.clear_color(0.6_f32, 0.8_f32, 1.0_f32, 1.0_f32)

    loop do
      now = LibGLFW.get_time
      delta_time = now - before

      puts "Iteration (dt = #{delta_time})"

      Scheduler.yield

      LibGLFW.poll_events

      if LibGLFW.get_key(@window, LibGLFW::KEY_ESCAPE) == LibGLFW::PRESS &&
        LibGLFW.window_should_close(@window)
        break
      end

      LibGL.clear(LibGL::COLOR_BUFFER_BIT | LibGL::DEPTH_BUFFER_BIT)
      update(delta_time)
      render(delta_time)
      LibGLFW.swap_buffers(@window)

      before = now
    end

    cleanup
    LibGLFW.terminate
  end
end
