require "gl"
require "lib_stb_image"
require "http/server"

require "./app"

class MyGame < App
  def update(delta_time)
  end

  def render(delta_time)
  end
end

game = MyGame.new(1024, 768, "The Game")

port = 8090
server = HTTP::Server.new(port) do |request|
  HTTP::Response.ok "text/plain", "Hello world!"
end

spawn do
  puts "[http] Listening on http://0.0.0.0:#{port}"
  server.listen
end

puts "[game] Running"
game.run
puts "[game] Finished"
