default: run

.PHONY: run
run: glove
	@echo "=== Running…"
	./glove
	@echo

.PHONY: clean
clean:
	@echo "=== Cleaning…"
	rm -rf .crystal
	rm -rf src/stb_image/*.o
	rm -f libstb_image.a
	rm -f glove
	@echo

src/stb_image/stb_image.o: src/stb_image/stb_image.h src/stb_image/stb_image.c
	@echo "=== Compiling stb_image…"
	clang -c src/stb_image/stb_image.c -o src/stb_image/stb_image.o
	@echo

libstb_image.a: src/stb_image/stb_image.o
	@echo "=== Linking stb_image…"
	libtool -static src/stb_image/stb_image.o -o libstb_image.a
	@echo

.PHONY: glove
glove: libstb_image.a
	@echo "=== Building glove…"
	crystal build -o glove src/glove/main.cr
	@echo
